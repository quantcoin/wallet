import alt from "alt-instance";
import accountUtils from "common/account_utils";
import AccountApi from "api/accountApi";

import WalletApi from "api/WalletApi";
import FaucetAPI from "api/FaucetAPI";
import ApplicationApi from "api/ApplicationApi";
import WalletDb from "stores/WalletDb";
import WalletActions from "actions/WalletActions";
import SettingsStore from "stores/SettingsStore";
import { ChainStore, FetchChain } from "bitsharesjs/es";

let accountSearch = {};
let wallet_api = new WalletApi();
let application_api = new ApplicationApi();

/**
 *  @brief  Actions that modify linked accounts
 *
 *  @note this class also includes accountSearch actions which keep track of search result state.  The presumption
 *  is that there is only ever one active "search result" at a time.
 */
class AccountActions {

    /**
     *  Account search results are not managed by the ChainStore cache so are
     *  tracked as part of the AccountStore.
     */
    accountSearch(start_symbol, limit = 50) {
        let uid = `${start_symbol}_${limit}}`;
        return (dispatch) => {
            if (!accountSearch[uid]) {
                accountSearch[uid] = true;
                return AccountApi.lookupAccounts(start_symbol, limit)
                    .then(result => {
                        accountSearch[uid] = false;
                        dispatch({ accounts: result, searchTerm: start_symbol });
                    });
            }
        };
    }

    setCurrentAccount(name) {
        return (dispatch) => {
            dispatch(name);
            return name;
        };
    }

    tryToSetCurrentAccount() {
        return (dispatch) => {
            return dispatch();
        };
    }

    requestAccountCreation(name, surname, document_type, document_number, email, password) {
        return (dispatch) => {
            let faucetAddress = SettingsStore.getSetting("faucet_address");

            if (window && window.location && window.location.protocol === "https:") {
                faucetAddress = faucetAddress.replace(/http:\/\//, "https://");
            }

            const keys = WalletDb.generateKeyFromPassword(email, "active", password);

            return fetch(faucetAddress + "/api/v1/accounts", {
                method: "post",
                mode: "cors",
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    first_name: name,
                    last_name: surname,
                    document: {
                        type: document_type,
                        number: document_number
                    },
                    email: email,
                    public_key: keys.pubKey
                })
            }).then(r => {
                return r.json();
            }).then((accountData) => {
                const accountName = accountData.name;
                accountData = {
                    accountName,
                    name: name,
                    surname,
                    document_type,
                    document_number,
                    email
                };
                dispatch(accountData);
                return accountName;
            });
        };
    }

    /**
     *  TODO:  This is a function of teh wallet_api and has no business being part of AccountActions
     */
    transfer(from_account, to_account, amount, asset, memo, propose_account = null, fee_asset_id = "1.3.0") {

        // Set the fee asset to use
        fee_asset_id = accountUtils.getFinalFeeAsset(propose_account || from_account, "transfer", fee_asset_id);

        try {
            return (dispatch) => {
                return application_api.transfer({
                    from_account, to_account, amount, asset, memo, propose_account, fee_asset_id
                }).then(result => {
                    // console.log( "transfer result: ", result )

                    dispatch(result);
                });
            };
        } catch (error) {
            console.log("[AccountActions.js:90] ----- transfer error ----->", error);
            return new Promise((resolve, reject) => {
                reject(error);
            });
        }
    }

    /**
     *  This method exists ont he AccountActions because after creating the account via the wallet, the account needs
     *  to be linked and added to the local database.
     */
    createAccount(
        account_data,
        registrar,
        referrer,
        referrer_percent,
        refcode
    ) {
        return (dispatch) => {
            return WalletActions.createAccount(
                account_data,
                registrar,
                referrer,
                referrer_percent,
                refcode
            ).then(() => {
                //todo
                dispatch(account_data);
                return account_data;
            });
        };
    }

    createAccountWithPassword(
        account_name,
        password,
        registrar,
        referrer,
        referrer_percent,
        refcode
    ) {
        return (dispatch) => {
            return WalletActions.createAccountWithPassword(
                account_name,
                password,
                registrar,
                referrer,
                referrer_percent,
                refcode
            ).then(() => {
                dispatch(account_name);
                return account_name;
            });
        };
    }

    /**
     *  TODO:  This is a function of the wallet_api and has no business being part of AccountActions, the account should already
     *  be linked.
     */
    upgradeAccount(account_id, lifetime) {
        // Set the fee asset to use
        let fee_asset_id = accountUtils.getFinalFeeAsset(account_id, "account_upgrade");

        var tr = wallet_api.new_transaction();
        tr.add_type_operation("account_upgrade", {
            "fee": {
                amount: 0,
                asset_id: fee_asset_id
            },
            "account_to_upgrade": account_id,
            "upgrade_to_lifetime_member": lifetime
        });
        return WalletDb.process_transaction(tr, null, true);
    }

    linkAccount(name) {
        return name;
    }

    unlinkAccount(name) {
        return name;
    }

    setPasswordAccount(account) {
        return account;
    }

    signOut() {
        return true;
    }

    fetchAccountProperties(activeAccountName) {
        return (dispatch) => {
            console.log("Fetching account", activeAccountName, "chain properties");

            return FetchChain("getAccount", activeAccountName)
                .then((account) => {
                    if (!account) {
                        throw Error(`${activeAccountName} is not registered in blockchain`);
                    }

                    return ChainStore.fetchCommitteeMemberByAccount(account.get("id")).then(committee => {
                        return { committee, account };
                    });
                }).then(res => {
                    dispatch(res);
                }).catch((err) => {
                    console.error(err);
                    dispatch(null);
                });
        };

    }

    checkAccountStatus(activeAccountName) {
        return (dispatch) => {
            FaucetAPI.getAccountRegistrationStatus(activeAccountName).then(res => {
                dispatch(res);
                return res;
            }).catch(err => {
                dispatch(err);
            });
        };
    }

}

export default alt.createActions(AccountActions);
