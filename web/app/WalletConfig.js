export default class WalletConfig {
    static getConfig() {
        return fetch("wallet-config.json")
                .then((res) => res.json())
                .then((data) => {
                    return data;
                });
    }
} 