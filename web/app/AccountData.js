export default class AccountData {
    constructor(account) {
        this.publicKey = account.public_key;
        this.email = account.email;
        this.name = account.name;
        this.firstName = account.first_name;
        this.approved = account.approved;
        this.created = account.created;
        this.documentNumber = account.document_number;
        this.ownerAccountId = account.owner_account_id;
        this.declined = account.disabled;
        this.ownerLogin = account.owner_login;
    }
}