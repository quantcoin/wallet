import SettingsStore from "../stores/SettingsStore";
import WalletUnlockActions from "../actions/WalletUnlockActions";
import WalletDb from "../stores/WalletDb";

import { FetchChain, Signature } from "bitsharesjs/es";

function toURLParametersString(params) {
    let searchParams = new URLSearchParams();
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            searchParams.set(key, params[key]);
        }
    }
    return searchParams.toString();
}


class FaucetAPI {

    constructor(registrarAccountName) {
        this.registrarAccountName = registrarAccountName;
    }

    static getFaucetAddress() {
        let faucetAddress = SettingsStore.getSetting("faucet_address");

        if (window && window.location && window.location.protocol === "https:") {
            faucetAddress = faucetAddress.replace(/http:\/\//, "https://");
        }
        return faucetAddress;
    }

    // POST: /api/v1/accounts/approve
    approveAccount(accountName) {
        const faucetAddress = FaucetAPI.getFaucetAddress();
        return this.getAuthorizationFields().then((params) => {

            const searchParams = toURLParametersString(params);

            return fetch(faucetAddress + "/api/v1/accounts/approve?" + searchParams, {
                method: "post",
                mode: "cors",
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    "name": accountName
                })
            }).then(r => r.json().then(res => {
                if (!res || (res && res.error)) {
                    throw new Error(res.error);
                } else {
                    return true;
                }
            })).catch((err) => {
                console.error(err);
                return err;
            });

        });
    }

    // POST: /api/v1/accounts
    requestAccountCreation(name, surname, documentType, documentNumber, email, publicKey) {
        const faucetAddress = FaucetAPI.getFaucetAddress();

        return fetch(faucetAddress + "/api/v1/accounts", {
            method: "post",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                first_name: name,
                last_name: surname,
                document: {
                    type: documentType,
                    number: documentNumber
                },
                email: email,
                public_key: publicKey
            })
        }).then(r => {
            return r.json();
        }).then((res) => {
            const accountName = res.name;
            // let account_object = {
            //     first_name: name,
            //     last_name: surname,
            //     document_number: document_number,
            //     email: email,
            //     account_name: account_name
            // };
            return accountName;
        });
    };

    // GET: /api/v1/accounts/approve
    static getAccountRegistrationStatus(accountName) {
        const faucetAddress = FaucetAPI.getFaucetAddress();
        let searchParams = toURLParametersString({ "name": accountName });

        return fetch(faucetAddress + `/api/v1/accounts/approve?${searchParams}`, {
            method: "get",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json"
            },
        }).then(r => {
            return r.json();
        }).then((res) => {
            return { accountName: res.name, approved: res.approved };
        });
    }

    //GET: /api/v1/accounts/account_name
    static getAccountWithEmail(email) {
        const faucetAddress = FaucetAPI.getFaucetAddress();
        let searchParams = toURLParametersString({ "email": email });

        return fetch(faucetAddress + `/api/v1/accounts/account_name_by_email?${searchParams}`, {
            method: "get",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json"
            },
        }).then(r => {
            return r.json();
        }).then((res) => {
            if (!res)
                return null;
            return { accountName: res.name };
        }).catch((err) => {
            console.error(`FaucetAPI::getAccountWithEmail() ${err}`);
            return null;
        });
    }

    //GET: /api/v1/accounts
    getRegistrationRequests(params) {
        const faucetAddress = FaucetAPI.getFaucetAddress();
        return this.getAuthorizationFields().then((result) => {
            params = {
                ...params,
                ...result
            };
            let searchParams = toURLParametersString(params);

            return fetch(faucetAddress + `/api/v1/accounts?${searchParams}`, {
                method: "get",
                mode: "cors",
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json"
                }
            });
        }).then(r => {
            return r.json();
        }).then(res => {
            if (!res || (res && res.error)) {
                throw new Error(res.error);
            } else {
                return res.accounts;
            }
        });
    }

    getAccounts(params) {
        return this.getRegistrationRequests(params);
    }

    getAccountWithAccountName(accountName) {
        return this.getRegistrationRequests({ name: accountName }).then((accounts) => {
            if (!accounts) {
                return null;
            } else {
                return accounts[0];
            }
        });
    }

    //DELETE: /api/v1/accounts/${accountName}
    declineRegistrationRequest(accountName) {
        const faucetAddress = FaucetAPI.getFaucetAddress();
        this.getAuthorizationFields().then((result) => {
            const params = toURLParametersString(result);

            return fetch(faucetAddress + `/api/v1/accounts/${accountName}?${params}`, {
                method: "DELETE",
                mode: "cors",
                headers: {
                    "Accept": "application/json"
                }
            });
        }).then(r => r.json())
            .then(res => {
                if (!res || (res && res.error)) {
                    throw new Error(res.error);
                } else {
                    return true;
                }
            }).catch((err) => {
                console.error(err);
                return false;
            });
    }

    getAuthorizationFields() {

        return FetchChain("getAccount", this.registrarAccountName).then((account) => {
            if (!account) {
                return null;
            }
            let active_pubkey = account.getIn(["active", "key_auths", 0, 0]);
            return WalletUnlockActions.unlock().then(() => {
                let private_key = WalletDb.getPrivateKey(active_pubkey);
                let time = Math.trunc(Date.now() /( 60 * 1000));
                let timebuf = new ArrayBuffer(4); //4 bytes
                let dataview = new DataView(timebuf);
                dataview.setUint32(0, time);
                let signed = Signature.signBuffer(new Buffer(timebuf), private_key);
                let signatureString = signed.toHex();
                return {
                    req_account: this.registrarAccountName,
                    req_signature: signatureString
                };
            });
        });
    }
}

export default FaucetAPI;