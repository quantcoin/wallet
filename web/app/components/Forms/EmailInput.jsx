import React from "react";
import DefaultForm from "./DefaultForm";

export default class NameInput extends React.Component {
    constructor() {
        super();
        this.errorFunctions = [
            {
                isValid: (value) => {
                    return value.length > 0;
                },
                message: "Строка не может быть пустой"
            },
            {
                isValid: (value) => {
                    return !value.match(/[^A-Za-z'-`]/);
                },
                message: "Можно использовать только латинский алфавит"
            }
        ];
    }

    render() {
        return (
            <DefaultForm
                regExp={"[A-Za-z]+"}
                onChange={this.onNameChange.bind(this)}
                placeholder={counterpart.translate("account.first_name")}
                errorFunctions={this.errorFunctions}
                noLabel
            />
        );
    }
}
