import React from "react";
import {PropTypes} from "react";
import classNames from "classnames";
import Translate from "react-translate-component";

export default class DefaultForm extends React.Component {

    static propTypes = {
        id: PropTypes.string,
        placeholder: PropTypes.string,
        initial_value: PropTypes.string,
        onChange: PropTypes.func,
        onEnter: PropTypes.func,
        noLabel: PropTypes.bool,
        regExp: PropTypes.string.isRequired,
        errorFunctions: PropTypes.array
    };

    static defaultProps = {
        noLabel: false,
        errorFunctions: []
    };

    constructor() {
        super();
        this.state = {
            value: null,
            error: null,
        };

        this.handleChange = this.handleChange.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.value !== this.state.value
            || nextState.error !== this.state.error;
    }

    componentDidUpdate() {
        if (this.props.onChange) this.props.onChange({value: this.state.value, valid: !this.getError()});
    }

    getValue() {
        return this.state.value;
    }

    setValue(value) {
        this.setState({value});
    }

    clear() {
        this.setState({ value: null, error: null, warning: null })
    }

    focus() {
        this.refs.input.focus();
    }

    valid() {
        return !this.getError();
    }

    getError() {
        if(this.state.value === null) return null;
        let error = null;
        if (this.state.error) {
            error = this.state.error;
        }
        return error;
    }

    validateValue(value) {
        for (let validator of this.props.errorFunctions) {
            if (!validator.isValid(value)) {
                return validator.message;
            }
        }

        return null;
    }

    handleChange(e) {
        e.preventDefault();
        e.stopPropagation();
        // Simplify the rules (prevent typing of invalid characters)
        var value = e.target.value;//.toLowerCase();
        value = value.match(this.props.regExp);//(/[A-Z][a-z]+/)
        value = value ? value[0] : null;

        const message = this.validateValue(value);
        this.setState({ value, message });
    }

    onKeyDown(e) {
        if (this.props.onEnter && event.keyCode === 13) this.props.onEnter(e);
    }

    render() {
        let error = this.getError() || "";
        let class_name = classNames("form-group", "account-name", {"has-error": false});
        let warning = this.state.warning;
        let {noLabel} = this.props;

        return (
            <div className={class_name}>
                {noLabel ? null : <label><Translate content="account.name" /></label>}
                <section>
                    <input
                        name="value"
                        type="text"
                        id={this.props.id}
                        ref="input"
                        autoComplete="off"
                        placeholder={this.props.placeholder}
                        onChange={this.handleChange}
                        onKeyDown={this.onKeyDown}
                        value={this.state.value || this.props.initial_value}
                    />
                </section>
                <div style={{textAlign: "left"}} className="facolor-error">{error}</div>
                <div style={{textAlign: "left"}} className="facolor-warning">{error ? null : warning}</div>
            </div>
        );
    }
}
