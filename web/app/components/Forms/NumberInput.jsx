import React from "react";
import DefaultForm from "./DefaultForm";

export default class NumberInput extends React.Component {
    constructor() {
        super();
        this.errorFunctions = [
            {
                isValid: (value) => {
                    return value && value.length > 0;
                },
                message: "Строка не может быть пустой"
            },
            {
                isValid: (value) => {
                    return !value.match(/[^0-9]/);
                },
                message: "Поле должно состоять только из цифр"
            }
        ];
    }

    valid() {
        return this.field.valid();
    }

    render() {
        return (
            <DefaultForm
                {...this.props}
                ref={(ref) => {this.field = ref;}}
                errorFunctions={this.errorFunctions}
            />
        );
    }
}
