import React from "react";
import { connect } from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import PasswordInput from "./../Forms/PasswordInput";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import { Link } from "react-router/es";
import LoadingIndicator from "../LoadingIndicator";
import Translate from "react-translate-component";
import { ChainStore, FetchChain } from "bitsharesjs/es";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import DefaultForm from "../Forms/DefaultForm";
import counterpart from "counterpart";
import RandomName from "random-name";

class CreateAccountPassword extends React.Component {
    constructor() {
        super();
        this.state = {
            validPassword: false,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1,
            showPass: false
        };
    }

    componentDidMount() {
        ReactTooltip.rebuild();
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: true
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state) || !utils.are_equal_shallow(nextProps, this.props);
    }

    isValid() {
        return true;
    }

    onPasswordChange(e) {
        this.setState({ validPassword: e.valid, pass: e.value });
    }

    onNameChange(e) {
        this.setState({ name: e.value });
    }

    onSurnameChange(e) {
        this.setState({ surname: e.value });
    }

    onEmailChange(e) {
        this.setState({ email: e.value });
    }

    onDocumentNumberChange(e) {
        this.setState({ documentNumber: e.value });
    }

    _unlockAccount(account, password) {
        WalletDb.validatePassword(password, true /* unlock */, account, ['active', 'owner', 'memo']);
        WalletUnlockActions.checkLock.defer();
    }

    createAccount(name, password) {
        this.setState({ loading: true });

        AccountActions.createAccountWithPassword(name, password, this.state.registrar_account, referralAccount || this.state.registrar_account, 0, refcode).then(() => {
            AccountActions.setPasswordAccount(name);

            FetchChain("getAccount", name).then(() => {
                this.setState({
                    step: 2
                });
            });
            this.props.router.push("/proposal_pending");

        }).catch(error => {
            console.log("ERROR AccountActions.createAccount", error);
            let error_msg = error.base && error.base.length && error.base.length > 0 ? error.base[0] : "unknown error";
            if (error.remote_ip) error_msg = error.remote_ip[0];
            notify.addNotification({
                message: `Failed to create account: ${name} - ${error_msg}`,
                level: "error",
                autoDismiss: 10
            });
            this.setState({ loading: false });
        });
    }

    requestAccountCreation() {
        AccountActions.requestAccountCreation(
            this.state.name,
            this.state.surname,
            "student card",
            this.state.documentNumber,
            this.state.email,
            this.state.pass
        ).then((accountName) => {
            this._unlockAccount(accountName, this.state.pass);
            AccountActions.setPasswordAccount(accountName);

            if (AccountStore.getState().registrationInfo) {
                this.props.router.push("/pending-registration");
            }
            notify.info("Registration request for " + accountName + " was sent. After the approvment you will receive an email notification");
        }).catch(err => {
            notify.error("Failed to send regsitration request");
            console.error(err);
        });
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.isValid()) return;
        this.requestAccountCreation();
    }

    onRegistrarAccountChange(registrar_account) {
        this.setState({ registrar_account });
    }

    _renderAccountCreateForm() {

        let { registrar_account } = this.state;

        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account ? ChainStore.getAccount(registrar_account) : null;
        if (registrar) {
            if (registrar.get("lifetime_referrer") == registrar.get("id")) {
                isLTM = true;
            }
        }

        let buttonClass = classNames("submit-button button no-margin", { disabled: (!valid || (registrar_account && !isLTM)) });

        return (
            <div>
                <form
                    style={{ maxWidth: "40rem" }}
                    onSubmit={this.onSubmit.bind(this)}
                    noValidate
                >
                    <p style={{ fontWeight: "bold" }}><Translate content="wallet.create_password" /></p>

                    <DefaultForm
                        ref={(ref) => { this.nameRef = ref; }}
                        regExp={"[A-Za-z]+"}
                        onChange={this.onNameChange.bind(this)}
                        placeholder={counterpart.translate("account.first_name")}
                        noLabel
                    />

                    <DefaultForm
                        ref={(ref) => { this.surnameRef = ref; }}
                        regExp={"[A-Za-z]+"}
                        onChange={this.onSurnameChange.bind(this)}
                        placeholder={counterpart.translate("account.surname")}
                        noLabel
                    />

                    <DefaultForm
                        ref={(ref) => { this.docNumberRef = ref; }}
                        regExp={"[0-9]+"}
                        onChange={this.onDocumentNumberChange.bind(this)}
                        placeholder={counterpart.translate("account.doc_number")}
                        noLabel
                    />

                    <DefaultForm
                        ref={(ref) => { this.emailRef = ref; }}
                        regExp={".+@.+"}
                        onChange={this.onEmailChange.bind(this)}
                        placeholder={counterpart.translate("account.email")}
                        noLabel
                    />



                    {/* Only ask for password if a wallet already exists */}
                    <PasswordInput
                        ref="password"
                        confirmation={true}
                        onChange={this.onPasswordChange.bind(this)}
                        noLabel
                        passwordLength={12}
                        checkStrength
                    />

                    <div className="divider" />

                    {/* Submit button */}
                    {this.state.loading ? <LoadingIndicator type="three-bounce" /> : <button className={buttonClass}><Translate content="account.create_account" /></button>}



                </form>
                <hr/>
                {__DEV__ ? <button onClick={this._autofill.bind(this)} className="button" style={{ margin: 20 }}>
                    RANDOMIZE
                </button> : null}
            </div>
        );
    }

    _autofill() {
        const firstName = RandomName.first();
        const lastName = RandomName.last();
        const email = `${firstName}.${lastName}@example.com`;
        const password = email;
        const documentNumber = Math.round(Math.random()*1000000);;

        this.nameRef.refs.input.value = firstName;
        this.surnameRef.refs.input.value = lastName;
        this.emailRef.refs.input.value = email;
        this.docNumberRef.refs.input.value = documentNumber;
        this.refs.password.refs.password.value = password;
        this.refs.password.refs.confirm_password.value = password;

        this.setState({
            validPassword: true,
            pass: password,
            name: firstName,
            surname: lastName,
            email: email,
            documentNumber: documentNumber
        });
    }

    _renderAccountCreateText() {
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div>
                <h4 style={{ fontWeight: "bold", paddingBottom: 15 }}><Translate content="wallet.wallet_password" /></h4>

                <Translate style={{ textAlign: "left" }} unsafe component="p" content="wallet.create_account_password_text" />

                <Translate style={{ textAlign: "left" }} component="p" content="wallet.create_account_text" />

                {firstAccount ?
                    <Translate style={{ textAlign: "left" }} component="p" content="wallet.first_account_paid" /> :
                    <Translate style={{ textAlign: "left" }} component="p" content="wallet.not_first_account" />}
            </div>
        );
    }

    _renderGetStarted() {

        return (
            <div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td><Translate content="wallet.tips_account" />:</td>
                            <td><Link to={`/account/${this.state.accountName}/overview`} ><Translate content="wallet.link_account" /></Link></td>
                        </tr>

                        <tr>
                            <td><Translate content="wallet.tips_deposit" />:</td>
                            <td><Link to="/deposit-withdraw"><Translate content="wallet.link_deposit" /></Link></td>
                        </tr>



                        <tr>
                            <td><Translate content="wallet.tips_transfer" />:</td>
                            <td><Link to="/transfer"><Translate content="wallet.link_transfer" /></Link></td>
                        </tr>

                        <tr>
                            <td><Translate content="wallet.tips_settings" />:</td>
                            <td><Link to="/settings"><Translate content="header.settings" /></Link></td>
                        </tr>
                    </tbody>

                </table>
            </div>
        );
    }

    _renderGetStartedText() {

        return (
            <div>
                <p style={{ fontWeight: "bold" }}><Translate content="wallet.congrat" /></p>

                <p><Translate content="wallet.tips_explore_pass" /></p>

                <p><Translate content="wallet.tips_header" /></p>

                <p className="txtlabel warning"><Translate content="wallet.tips_login" /></p>
            </div>
        );
    }

    render() {
        // let my_accounts = AccountStore.getMyAccounts();
        // let firstAccount = my_accounts.length === 0;
        return (
            <div className="grid-block vertical page-layout Account_create">
                <div className="grid-block shrink small-12 medium-10 medium-offset-2">
                    <div className="grid-content" style={{ paddingTop: 20 }}>
                        <Translate content="wallet.wallet_new" component="h2" />
                        {/* <h4 style={{paddingTop: 20}}>
                            {step === 1 ?
                                <span>{firstAccount ? <Translate content="wallet.create_w_a" />  : <Translate content="wallet.create_a" />}</span> :
                            step === 2 ? <Translate content="wallet.create_success" /> :
                            <Translate content="wallet.all_set" />
                            }
                        </h4> */}
                    </div>
                </div>
                <div className="grid-block wrap">
                    <div className="grid-content small-12 medium-4 medium-offset-2">
                        <p style={{ fontWeight: "bold" }}>
                            <Translate content={"wallet.step_1"} />
                        </p>
                        { this._renderAccountCreateForm() }
                    </div>

                    <div className="grid-content small-12 medium-4 medium-offset-1">
                        {this._renderAccountCreateText()}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(CreateAccountPassword, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {
            currentAccount: AccountStore.getState().currentAccount
        };
    }
});
