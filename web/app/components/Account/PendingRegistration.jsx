import React from "react";
import Translate from "react-translate-component";
import AccountActions from "../../actions/AccountActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountStore from "../../stores/AccountStore";


export default class PendingRegistration extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            registrationInfo: AccountStore.getState().registrationInfo
        };

        this.onStoreChanged = this.onStoreChanged.bind(this);
    }

    componentDidMount() {
        this.checkInterval = setInterval(() => {
            return AccountActions.checkAccountStatus(this.state.registrationInfo.accountName);
        }, 5000);

        AccountStore.listen(this.onStoreChanged);
    }

    componentWillUnmount() {
        this.checkInterval && clearInterval(this.checkInterval);
        AccountStore.unlisten(this.onStoreChanged);
    }

    onStoreChanged(state) {
        if (!state.registrationInfo) {
            setTimeout(() => {
                WalletUnlockActions.unlock().then(() => {
                    this.props.router.push("/account/" + this.state.registrationInfo.accountName);
                });
            }, 0);
        } else {
            this.setState({
                registrationInfo: state.registrationInfo
            });
        }
    }

    render() {
        const registrationInfo = this.state.registrationInfo;
        return (
            <div style={{ margin: "20px" }}>
                <h4 style={{ textAlign: 'center' }}><Translate component="span" content="account.registration_check_later" /></h4>

                <div className="grid-block vertical large-8 large-offset-2" style={{ margin: "20px" }}>
                    <div className="grid-content">
                        <table className="table key-value-table">
                            <tbody>
                                <tr>
                                    <td><Translate content="account.name" /></td>
                                    <td>{registrationInfo.accountName}</td>
                                </tr>
                                <tr>
                                    <td><Translate content="account.first_name" /></td>
                                    <td>{registrationInfo.name}</td>
                                </tr>
                                <tr>
                                    <td><Translate content="account.surname" /></td>
                                    <td>{registrationInfo.surname}</td>
                                </tr>
                                <tr>
                                    <td><Translate content="account.doc_number" /></td>
                                    <td>{registrationInfo.document_number}</td>
                                </tr>
                                <tr>
                                    <td><Translate content="account.email" /></td>
                                    <td>{registrationInfo.email}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}