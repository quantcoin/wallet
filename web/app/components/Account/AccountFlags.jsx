import React from "react";
import Immutable from "immutable";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import utils from "common/utils";
import accountUtils from "common/account_utils";
import ApplicationApi from "../../api/ApplicationApi";
import AccountStore from "../../stores/AccountStore";
import { connect } from "alt-react";
import { ChainStore, FetchChainObjects, FetchChain } from "bitsharesjs/es";

let applicationApi = new ApplicationApi();

class AccountFlags extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeAccount: null,
            registrationFlag: this.props.account.get("flags") & 0b1
        };
    }

    componentDidMount() {
        FetchChain("getAccount", this.props.activeAccountName)
            .then((account) => {
                this.setState({ activeAccount: account });
            });
    }

    onToggleRegistrationRights() {
        this.setState({
            registrationFlag: (this.props.account.get("flags") ^ 0b1) & 0b1
        });
    }

    onSubmit() {
        if (this.props.account.get("flags") & 0b1 === this.state.registrationFlag)
            return;

        applicationApi.update_account_flags(this.props.account.get("id"),
            this.state.activeAccount.get("id"),
            this.state.registrationFlag, true);
    }

    render() {
        let tabIndex = 1;
        return (
            <div>
                <div className="content-block">
                    <h3><Translate content={"account.flags"} /></h3>
                </div>
                <div className="content-block">
                    <div>
                        <label
                            className="inline-block"
                            style={{
                                position: "relative",
                                top: -10,
                                margin: 0
                            }}
                            data-place="bottom">
                            <span><Translate content="account.registration_permission" />:&nbsp;&nbsp;</span>
                        </label>
                        <div className="switch" onClick={() => { this.onToggleRegistrationRights(); }}>
                            <input type="checkbox" ref="checkbox" checked={this.state.registrationFlag} tabIndex={tabIndex++} />
                            <label />
                        </div>
                    </div>
                </div>

                <div className="content-block">
                    <button className="button outline" onClick={this.onSubmit.bind(this)} tabIndex={tabIndex++} disabled={!this.props.account}>
                        <Translate content="account.perm.publish" />
                    </button>
                </div>
            </div>

        );
    }

}

export default connect(AccountFlags, {
    listenTo() {
        return [AccountStore];
    },

    getProps() {
        return {
            activeAccountName: AccountStore.getState().currentAccount,
        };
    },
});