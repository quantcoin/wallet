import React from "react";
import { connect } from "alt-react";
import Translate from "react-translate-component";
import AccountStore from "../../stores/AccountStore";
import notify from "actions/NotificationActions";
import WalletDb from "../../stores/WalletDb";
import FaucetAPI from "../../api/FaucetAPI";
import { TransactionBuilder } from "bitsharesjs/es";

class PendingRequests extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            requests: [],
            filtered_requests: []
        };
    }

    componentDidMount() {
        this._fetchPendingUserRegistrationsFromFaucet();
        this.timer = setInterval(this._fetchPendingUserRegistrationsFromFaucet.bind(this), 10 * 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    _fetchPendingUserRegistrationsFromFaucet() {
        (new FaucetAPI(AccountStore.getState().currentAccount)).getRegistrationRequests().then(accounts => {
            this.setState({
                requests: accounts,
                filtered_requests: accounts.filter(this._filterRequest)
            });
        }).catch((err) => {
            console.error(err);
        });
    }

    /**
     * register user from current account's balance
     * owner_pub_key
     * active_pub_key
     * account_name
     * registrar_name
     */
    registerUser(new_account_name, owner_account_id, active_pub_key) {
        let registrar = this.props.registrar;

        return new Promise((resolve, reject) => {
            let tr = new TransactionBuilder();
            tr.add_type_operation("account_create", {
                fee: {
                    amount: 0,
                    asset_id: 0
                },
                "registrar": registrar.get("id"),
                "referrer": registrar.get("id"),
                "referrer_percent": 0,
                "name": new_account_name,
                "owner": {
                    "weight_threshold": 1,
                    "account_auths": [[owner_account_id, 1]],
                    "key_auths": [],
                    "address_auths": []
                },
                "active": {
                    "weight_threshold": 1,
                    "account_auths": [],
                    "key_auths": [[active_pub_key, 1]],
                    "address_auths": []
                },
                "options": {
                    "memo_key": active_pub_key,
                    "voting_account": "1.2.5",
                    "num_witness": 0,
                    "num_committee": 0,
                    "votes": []
                }
            });
            // TransactionConfirmStore.listen(onTrxIncluded)
            return WalletDb.process_transaction(
                tr,
                null, //signer_private_keys,
                true
            ).then((res) => {
                this._notifyFaucetOfRegisteringAccountWithName(new_account_name);
                console.log("process_transaction then", res);
                resolve();
            }).then(() => {
                notify.info("Account " + new_account_name + " was registered on blockchain");
            }).then(resolve)
                .catch(err => {
                    notify.error("Failed to register account " + new_account_name);
                    console.log("process_transaction catch", err);
                    reject(err);
                });
        });
    }

    _notifyFaucetOfRegisteringAccountWithName(accountName) {
        (new FaucetAPI(AccountStore.getState().currentAccount)).approveAccount(accountName).then(() => {
            return this._fetchPendingUserRegistrationsFromFaucet();
        }).catch((err) => {
            console.error(err);
        });
    }

    _removeRequest(accountName) {
        (new FaucetAPI(AccountStore.getState().currentAccount)).declineRegistrationRequest(accountName).then(() => {
            return this._fetchPendingUserRegistrationsFromFaucet();
        }).catch((err) => {
            console.error(err);
            return err;
        });
    }

    /**
     * account_request
     *      'public_key': self.public_key,
            'email': self.email,
            'name': self.name,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'approved': self.approved,
            'created': self.created,
            'document_number': self.document_number,
            'owner_account_id': self.owner_account_id
     */

    _renderEmptyState() {
        return (
            <div className="grid-block vertical large-8 large-offset-2" style={{ margin: "20px" }}>
                <div className="grid-content">
                    <h4 style={{textAlign: "center"}}><Translate component="span" content="account.requests_empty" /></h4>
                </div>
            </div>
        );
    }

    _renderFilled(requestsItems) {
        return (
            <table className="table requests-table" style={{ width: "100%" }}>
                <thead>
                    <tr>
                        <th style={{ textAlign: "left" }}>
                            <Translate component="span" content="account.name" />
                        </th>
                        <th style={{ textAlign: "left" }}>
                            <Translate component="span" content="account.first_name" />
                        </th>
                        <th style={{ textAlign: "left" }} className="column-hide-small">
                            <Translate component="span" content="account.surname" />
                        </th>
                        <th style={{ textAlign: "left" }}>
                            <Translate content="account.doc_number" />
                        </th>
                        <th style={{ textAlign: "left" }}>
                            <Translate content="account.email" />
                        </th>
                        <th style={{ textAlign: "left" }}>
                            <Translate content="account.creation_date" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {requestsItems}
                </tbody>
            </table>
        );
    }

    _filterRequest(account_request) {
        return !account_request.approved && !account_request.disabled;
    }

    render() {
        if (this.state.filtered_requests.length) {
            let requestItems = this.state.filtered_requests.map((account_request) => {
                return (
                    <PendingUser key={account_request.name}
                        first_name={account_request.first_name}
                        surname={account_request.last_name}
                        account_name={account_request.name}
                        document_number={account_request.document_number}
                        email={account_request.email}
                        creation_date={account_request.created}
                        onRegisterUserClick={() => this.registerUser(account_request.name,
                            account_request.owner_account_id,
                            account_request.public_key)}
                        onRemoveRequestClick={() => this._removeRequest(account_request.name)}>
                    </PendingUser>
                );
            });
            return this._renderFilled(requestItems);
        }

        else {
            return this._renderEmptyState();
        }
    }

}

PendingRequests = connect(PendingRequests, {
    listenTo() {
        return [AccountStore];
    },

    getProps() {
        return {
            registrar: AccountStore.getState().currentAccountProperties.account
        };
    }
});

class PendingUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <tr>
                <td style={{ textAlign: "left" }}>
                    {this.props.account_name}
                </td>
                <td style={{ textAlign: "left" }}>
                    {this.props.first_name}
                </td>
                <td style={{ textAlign: "left" }}>
                    {this.props.surname}
                </td>
                <td style={{ textAlign: "left" }}>
                    {this.props.document_number}
                </td>
                <td style={{ textAlign: "left" }}>
                    {this.props.email}
                </td>
                <td style={{ textAlign: "left" }}>
                    {this.props.creation_date}
                </td>

                {this.props.onRegisterUserClick ? <td className="noborder">
                    <div className="account-info more-button">
                        <button className="button outline small"
                            onClick={() => this.props.onRegisterUserClick()}>
                            <Translate content="account.register" />
                        </button>
                    </div>
                </td> : null}
                {this.props.onRemoveRequestClick ? <td className="noborder">
                    <div className="account-info more-button">
                        <button className="button outline small"
                            onClick={() => this.props.onRemoveRequestClick()}>
                            <Translate content="account.remove" />
                        </button>
                    </div>
                </td> : null}
            </tr>
        );
    }
}

export default PendingRequests;
