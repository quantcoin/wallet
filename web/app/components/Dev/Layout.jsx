import React, { Component } from "react";


export default class Layout extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
    }
    render() {
        return (
            <div className="content-block">
                {this.props.children}
            </div>
        );
    }
}