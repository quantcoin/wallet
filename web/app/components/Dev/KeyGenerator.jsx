import React, { Component } from "react";
import WalletDb from "stores/WalletDb";

class KeyGenerator extends Component {
    constructor() {
        super();
        this.state = {
            privateKey: "",
            publicKey: ""
        };
    }

    generate() {
        const keys = WalletDb.generateKeyFromPassword(this.refs.email.value, this.refs.role.value, this.refs.password.value);
        this.setState({
            privateKey: keys.privKey.toWif(),
            publicKey: keys.pubKey
        });
    }

    render() {
        return (
            <div>
                <div>
                    <label>E-mail</label>
                    <input ref='email' type='text' />
                    <label>Password</label>
                    <input ref='password' type='text' />
                    <label>Role</label>
                    <input ref='role' type='text' />
                    <button onClick={this.generate.bind(this)} className="button outline">Generate</button>
                </div>
                <div>
                    Private key: {this.state.privateKey}
                </div>
                <div>
                    Public key: {this.state.publicKey}
                </div>

            </div>
        );
    }
}

export default KeyGenerator;
